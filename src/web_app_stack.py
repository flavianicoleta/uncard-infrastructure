from aws_cdk.core import Stack, Construct, CfnOutput
from aws_cdk.aws_amplify import CfnApp

from src.constants import UncardConstants

class UncardWebAppStack(Stack):
    def __init__(self, scope: Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id, **kwargs)

        # TODO: might need to create a role
        # CfnServiceLinkedRole(self, "EcsServiceLinkedRole",
        #                      aws_service_name="amplify.amazonaws.com")
        self.__web_app = CfnApp(self, "WebApp",
                                name=UncardConstants.web_app_name)
        self.web_app_export = CfnOutput(self, "WebAppExport",
                                        value=self.__web_app.attr_app_id,
                                        export_name=UncardConstants.web_app_export)
