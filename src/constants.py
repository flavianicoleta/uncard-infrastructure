
class UncardConstants:
    vpc_cidr = "172.16.0.0/24"
    private_subnet_1_cidr = "172.16.0.96/27"
    private_subnet_2_cidr = "172.16.0.128/27"
    private_subnet_3_cidr = "172.16.0.160/27"

    ecs_task_role_name = "UncardEcsTaskRole"
    ecs_execution_role_name = "UncardEcsExecutionRole"

    web_app_name = "uncard-web-app"
    web_app_url = "https://master.dj5rkicmdtfm3.amplifyapp.com/"

    ecr_repository_name = "uncard/service"

    user_pool_name = "uncard-user-directory"
    cognito_domain = "uncard"

    card_table_name = "card"

    ecs_cluster_name = "uncard-cluster"

    log_group_name = "uncard-logs"

    load_balancer_name = "uncard-nlb"
    target_group_name = "uncard-tg"
    service_name = "uncard-fargate-service"

    user_pool_export = "UncardUserPool"
    user_pool_id_export = "UncardUserPoolId"
    user_pool_web_client_id_export = "UncardUserPoolWebClientId"
    vpc_link_export = "UncardVpcLink"
    load_balancer_dns_name_export = "UncardDnsName"
    rest_api_export = "UncardRestApi"
    web_app_export = "UncardWebApp"

    env = {'region': 'eu-west-1', 'account': '313059860395'}
