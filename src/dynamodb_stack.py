from aws_cdk.core import Stack, Construct
from aws_cdk.aws_dynamodb import CfnTable

from src.constants import UncardConstants


class UncardDynamoDbStack(Stack):
    def __init__(self, scope: Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id, **kwargs)

        self.__card_table = CfnTable(self, "Card",
                                     table_name=UncardConstants.card_table_name,
                                     provisioned_throughput={
                                         "readCapacityUnits": 5,
                                         "writeCapacityUnits": 5
                                     },
                                     attribute_definitions=[
                                         {
                                             "attributeName": "Id",
                                             "attributeType": "S"
                                         },
                                         {
                                             "attributeName": "Name",
                                             "attributeType": "S"
                                         }
                                     ],
                                     key_schema=[
                                         {
                                             "attributeName": "Id",
                                             "keyType": "HASH"
                                         }
                                     ],
                                     global_secondary_indexes=[
                                         {
                                             "indexName": "NameIndex",
                                             "keySchema": [
                                                 {
                                                     "attributeName": "Name",
                                                     "keyType": "HASH"
                                                 },
                                                 {
                                                     "attributeName": "Id",
                                                     "keyType": "RANGE"
                                                 }
                                             ],
                                             "projection": {
                                                 "projectionType": "ALL"
                                             },
                                             "provisionedThroughput": {
                                                 "readCapacityUnits": 5,
                                                 "writeCapacityUnits": 5
                                             }
                                         }
                                     ])
