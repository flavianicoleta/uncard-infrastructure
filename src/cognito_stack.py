from aws_cdk.core import Stack, Construct, CfnOutput
from aws_cdk.aws_cognito import UserPool, VerificationEmailStyle, Mfa, AccountRecovery, OAuthScope, \
    UserPoolClientIdentityProvider
from aws_cdk.core import Duration

from src.constants import UncardConstants


class UncardCognitoStack(Stack):
    def __init__(self, scope: Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id, **kwargs)

        self.__user_pool = UserPool(self, "UserPool",
                                    user_pool_name=UncardConstants.user_pool_name,
                                    self_sign_up_enabled=True,
                                    sign_in_aliases={
                                        "email": True
                                    },
                                    sign_in_case_sensitive=False,
                                    auto_verify={"email": True},
                                    account_recovery=AccountRecovery.EMAIL_ONLY,
                                    user_verification={
                                        "email_subject": "Verify your email for our awesome app!",
                                        "email_body": "Hello, "
                                                      "Thanks for signing up to our awesome app! "
                                                      "Your verification code is {####}",
                                        "email_style": VerificationEmailStyle.CODE},
                                    standard_attributes={
                                        "email": {
                                            "required": True,
                                            "mutable": False
                                        },
                                        "fullname": {
                                            "required": True,
                                            "mutable": False
                                        }

                                    },
                                    password_policy={
                                        "min_length": 8,
                                        "require_lowercase": True,
                                        "require_uppercase": True,
                                        "require_digits": True,
                                        "require_symbols": True,
                                        "temp_password_validity": Duration.days(3)
                                    },
                                    mfa=Mfa.OFF,
                                    mfa_second_factor={
                                        "sms": False,
                                        "otp": False
                                    }
                                    )
        self.__user_pool_web_client = self.__user_pool.add_client("WebApp",
                                                                  supported_identity_providers=[
                                                                      UserPoolClientIdentityProvider.COGNITO],
                                                                  auth_flows={"user_password": True,
                                                                              "refresh_token": True},
                                                                  o_auth={
                                                                      "flows": {
                                                                          "authorization_code_grant": True,
                                                                          "implicit_code_grant": True
                                                                      },
                                                                      "scopes": [OAuthScope.OPENID, OAuthScope.EMAIL,
                                                                                 OAuthScope.PROFILE,
                                                                                 OAuthScope.COGNITO_ADMIN],
                                                                      "callback_urls": [UncardConstants.web_app_url]
                                                                  })

        self.__user_pool.add_domain("Domain",
                                    cognito_domain={
                                        "domain_prefix": UncardConstants.cognito_domain
                                    })

        self.user_pool_export = CfnOutput(self, "UserPoolExport",
                                          value=self.__user_pool.user_pool_arn,
                                          export_name=UncardConstants.user_pool_export)
        self.user_pool_export = CfnOutput(self, "UserPoolIdExport",
                                          value=self.__user_pool.user_pool_id,
                                          export_name=UncardConstants.user_pool_id_export)
        self.user_pool_web_client_id_export = CfnOutput(self, "UserPoolWebClientIdExport",
                                                        value=self.__user_pool_web_client.user_pool_client_id,
                                                        export_name=UncardConstants.user_pool_web_client_id_export)
