from aws_cdk.aws_iam import CfnRole
from aws_cdk.core import Stack, Construct

from src.constants import UncardConstants


class UncardRolesStack(Stack):
    def __init__(self, scope: Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id, **kwargs)
        # This is an IAM role which authorizes ECS to manage resources on your
        # account on your behalf, such as updating your load balancer with the
        # details of where your containers are, so that traffic can reach your
        # containers.
        self.__ecs_service_role = CfnRole(self, "EcsServiceRole",
                                          role_name=UncardConstants.ecs_execution_role_name,
                                          assume_role_policy_document={"Statement": [
                                              {
                                                  "Effect": "Allow",
                                                  "Principal": {
                                                      "Service": [
                                                          "ecs.amazonaws.com", "ecs-tasks.amazonaws.com"
                                                      ]
                                                  },
                                                  "Action": ["sts:AssumeRole"]
                                              }
                                          ]},
                                          path="/",
                                          policies=[
                                              {
                                                  "policyName": "ecs-service",
                                                  "policyDocument": {
                                                      "Statement": [
                                                          {
                                                              "Effect": "Allow",
                                                              "Action": [
                                                                  # Rules which allow ECS to attach network interfaces to instances
                                                                  # on your behalf in order for awsvpc networking mode to work right
                                                                  "ec2:AttachNetworkInterface",
                                                                  "ec2:CreateNetworkInterface",
                                                                  "ec2:CreateNetworkInterfacePermission",
                                                                  "ec2:DeleteNetworkInterface",
                                                                  "ec2:DeleteNetworkInterfacePermission",
                                                                  "ec2:Describe*",
                                                                  "ec2:DetachNetworkInterface",

                                                                  # Rules which allow ECS to update load balancers on your behalf
                                                                  # with the information sabout how to send traffic to your containers
                                                                  "elasticloadbalancing:DeregisterInstancesFromLoadBalancer",
                                                                  "elasticloadbalancing:DeregisterTargets",
                                                                  "elasticloadbalancing:Describe*",
                                                                  "elasticloadbalancing:RegisterInstancesWithLoadBalancer",
                                                                  "elasticloadbalancing:RegisterTargets",

                                                                  # Rules which allow ECS to run tasks that have IAM roles assigned to them.
                                                                  "iam:PassRole",

                                                                  # Rules that let ECS interact with container images.
                                                                  "ecr:GetAuthorizationToken",
                                                                  "ecr:BatchCheckLayerAvailability",
                                                                  "ecr:GetDownloadUrlForLayer",
                                                                  "ecr:BatchGetImage",

                                                                  # Rules that let ECS create and push logs to CloudWatch.
                                                                  "logs:DescribeLogStreams",
                                                                  "logs:CreateLogStream",
                                                                  "logs:CreateLogGroup",
                                                                  "logs:PutLogEvents"
                                                              ],
                                                              "Resource": "*"
                                                          }
                                                      ]
                                                  }
                                              }
                                          ])

        # This is a role which is used by the ECS tasks. Tasks in Amazon ECS define
        # the containers that should be deployed togehter and the resources they
        # require from a compute/memory perspective. So, the policies below will define
        # the IAM permissions that our Mythical Mysfits docker containers will have.
        # If you attempted to write any code for the Mythical Mysfits service that
        # interacted with different AWS service APIs, these roles would need to include
        # those as allowed actions.
        self.__ecs_task_role = CfnRole(self, "EcsTaskRole",
                                       role_name=UncardConstants.ecs_task_role_name,
                                       assume_role_policy_document={"Statement": [
                                           {
                                               "Effect": "Allow",
                                               "Principal": {
                                                   "Service": ["ecs-tasks.amazonaws.com"]
                                               },
                                               "Action": ["sts:AssumeRole"]
                                           }
                                       ]},
                                       path="/",
                                       policies=[
                                           {
                                               "policyName": "AmazonECSTaskRolePolicy",
                                               "policyDocument": {
                                                   "Statement": [
                                                       {
                                                           "Effect": "Allow",
                                                           "Action": [
                                                               # Allow the ECS Tasks to download images from ECR
                                                               "ecr:GetAuthorizationToken",
                                                               "ecr:BatchCheckLayerAvailability",
                                                               "ecr:GetDownloadUrlForLayer",
                                                               "ecr:BatchGetImage",

                                                               # Allow the ECS tasks to upload logs to CloudWatch
                                                               "logs:DescribeLogStreams",
                                                               "logs:CreateLogStream",
                                                               "logs:CreateLogGroup",
                                                               "logs:PutLogEvents"
                                                           ],
                                                           "Resource": "*"
                                                       },
                                                       {
                                                           "Effect": "Allow",
                                                           "Action": [
                                                               # Allows the ECS tasks to interact with only the MysfitsTable
                                                               # in DynamoDB
                                                               "dynamodb:Scan",
                                                               "dynamodb:Query",
                                                               "dynamodb:UpdateItem",
                                                               "dynamodb:GetItem"
                                                           ],
                                                           "Resource": "arn:aws:dynamodb:*:*:table/UncardTable*"
                                                       }
                                                   ]
                                               }
                                           }
                                       ])

