from aws_cdk.core import Stack, Construct
from aws_cdk.aws_ecr import CfnRepository

from src.constants import UncardConstants


class UncardEcrStack(Stack):
    def __init__(self, scope: Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id, **kwargs)

        self.__ecr_repository = CfnRepository(self, "EcrRepository",
                                              repository_name=UncardConstants.ecr_repository_name)
