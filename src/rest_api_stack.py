from aws_cdk.core import Stack, Construct, Fn, CfnOutput
from aws_cdk.aws_apigateway import CfnVpcLink, CfnRestApi, EndpointType, CfnDeployment

from src.constants import UncardConstants
from src.vpc_stack import UncardVpcStack


class UncardRestApiStack(Stack):
    def __init__(self, scope: Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id, **kwargs)

        self.__rest_api = CfnRestApi(self, "RestApi",
                                     endpoint_configuration=EndpointType.REGIONAL,
                                     fail_on_warnings=True,
                                     body={
                                         "swagger": 2.0,
                                         "info": {
                                             "title": "UncardApi"
                                         },
                                         "securityDefinitions": {
                                             "UncardUserPoolAuthorizer": {
                                                 "type": "apiKey",
                                                 "name": "Authorization",
                                                 "in": "header",
                                                 "x-amazon-apigateway-authtype": "cognito_user_pools",
                                                 "x-amazon-apigateway-authorizer": {
                                                     "type": "COGNITO_USER_POOLS",
                                                     "providerARNs": [
                                                         Fn.import_value(UncardConstants.user_pool_export)
                                                     ]
                                                 }
                                             }
                                         },
                                         "paths": {
                                             "/uncard": {
                                                 "get": {
                                                     "consumes": [
                                                         "application/json"
                                                     ],
                                                     "produces": [
                                                         "application/json"
                                                     ],
                                                     "parameters": [
                                                         {
                                                             "name": "Access-Control-Allow-Origin",
                                                             "in": "header",
                                                             "required": False,
                                                             "type": "string"
                                                         },
                                                         {
                                                             "name": "Access-Control-Allow-Headers",
                                                             "in": "header",
                                                             "required": False,
                                                             "type": "string"
                                                         }
                                                     ],
                                                     "responses": {
                                                         "200": {
                                                             "description": "Default response for CORS method",
                                                             "headers": {
                                                                 "Access-Control-Allow-Headers": {
                                                                     "type": "string"
                                                                 },
                                                                 "Access-Control-Allow-Methods": {
                                                                     "type": "string"
                                                                 },
                                                                 "Access-Control-Allow-Origin": {
                                                                     "type": "string"
                                                                 }
                                                             }
                                                         }
                                                     },
                                                     "x-amazon-apigateway-integration": {
                                                         "connectionType": "VPC_LINK",
                                                         "connectionId": Fn.import_value(
                                                             UncardConstants.vpc_link_export),
                                                         "httpMethod": "GET",
                                                         "type": "HTTP_PROXY",
                                                         "uri": Fn.join("", ["http://",
                                                                             Fn.import_value(
                                                                                 UncardConstants.load_balancer_dns_name_export),
                                                                             "/uncard"]),
                                                         "responses": {
                                                             "default": {
                                                                 "statusCode": "200",
                                                                 "responseParameters": {
                                                                     "method.response.header.Access-Control-Allow-Headers": "'Content-Type,X-Amz-Date,Authorization,X-Api-Key'",
                                                                     "method.response.header.Access-Control-Allow-Methods": "'*'",
                                                                     "method.response.header.Access-Control-Allow-Origin": "'*'"
                                                                 }
                                                             }
                                                         }
                                                     }
                                                 },
                                                 "options": {
                                                     "summary": "CORS support",
                                                     "description": "Enable CORS by returning correct headers\n",
                                                     "consumes": [
                                                         "application/json"
                                                     ],
                                                     "produces": [
                                                         "application/json"
                                                     ],
                                                     "tags": [
                                                         "CORS"
                                                     ],
                                                     "x-amazon-apigateway-integration": {
                                                         "type": "mock",
                                                         "requestTemplates": {
                                                             "application/json": "{\n  \"statusCode\" : 200\n}\n"
                                                         },
                                                         "responses": {
                                                             "default": {
                                                                 "statusCode": "200",
                                                                 "responseParameters": {
                                                                     "method.response.header.Access-Control-Allow-Headers": "'Content-Type,X-Amz-Date,Authorization,X-Api-Key'",
                                                                     "method.response.header.Access-Control-Allow-Methods": "'*'",
                                                                     "method.response.header.Access-Control-Allow-Origin": "'*'"
                                                                 },
                                                                 "responseTemplates": {
                                                                     "application/json": "{}\n"
                                                                 }
                                                             }
                                                         }
                                                     },
                                                     "responses": {
                                                         "200": {
                                                             "description": "Default response for CORS method",
                                                             "headers": {
                                                                 "Access-Control-Allow-Headers": {
                                                                     "type": "string"
                                                                 },
                                                                 "Access-Control-Allow-Methods": {
                                                                     "type": "string"
                                                                 },
                                                                 "Access-Control-Allow-Origin": {
                                                                     "type": "string"
                                                                 }
                                                             }
                                                         }
                                                     }
                                                 }
                                             }
                                         }
                                     })

        self.__deployment = CfnDeployment(self, "Deployment",
                                          rest_api_id=self.__rest_api.ref,
                                          stage_name="dev")

        self._rest_api_export = CfnOutput(self, "RestApiExport",
                                          value=self.__rest_api.ref,
                                          export_name=UncardConstants.rest_api_export)
