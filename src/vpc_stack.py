from aws_cdk.aws_ec2 import CfnInternetGateway, CfnVPC, CfnRouteTable, CfnSubnet, CfnVPCGatewayAttachment, CfnRoute, \
    CfnSubnetRouteTableAssociation, CfnEIP, CfnNatGateway, CfnVPCEndpoint, CfnSecurityGroup
from aws_cdk.core import Fn, Aws, Stack, Construct, CfnOutput
from aws_cdk.aws_ecs import CfnCluster, CfnTaskDefinition, CfnService
from aws_cdk.aws_logs import CfnLogGroup
from aws_cdk.aws_apigateway import CfnVpcLink, CfnRestApi, EndpointType, CfnDeployment
from aws_cdk.aws_elasticloadbalancingv2 import CfnLoadBalancer, CfnTargetGroup, CfnListener

from src.constants import UncardConstants


class UncardVpcStack(Stack):
    def __init__(self, scope: Construct, id: str, **kwargs) -> None:
        super().__init__(scope, id, **kwargs)
        region = kwargs["env"]["region"]

        vpc = CfnVPC(self, "VPC",
                     cidr_block=UncardConstants.vpc_cidr,
                     enable_dns_support=True,
                     enable_dns_hostnames=True,
                     )
        route_table = CfnRouteTable(self, "PrivateRouteTable",
                                    vpc_id=vpc.ref)

        private_subnets = [
            CfnSubnet(self, "PrivateSubnet1",
                      cidr_block=UncardConstants.private_subnet_1_cidr,
                      vpc_id=vpc.ref,
                      availability_zone=Fn.select(0, Fn.get_azs(region))),
            CfnSubnet(self, "PrivateSubnet2",
                      cidr_block=UncardConstants.private_subnet_2_cidr,
                      vpc_id=vpc.ref,
                      availability_zone=Fn.select(1, Fn.get_azs(region))),
            CfnSubnet(self, "PrivateSubnet3",
                      cidr_block=UncardConstants.private_subnet_3_cidr,
                      vpc_id=vpc.ref,
                      availability_zone=Fn.select(2, Fn.get_azs(region)))
        ]

        private_route_table_association = [
            CfnSubnetRouteTableAssociation(self, "PrivateSubnet1RouteTableAssociation",
                                           route_table_id=route_table.ref,
                                           subnet_id=private_subnets[0].ref),
            CfnSubnetRouteTableAssociation(self, "PrivateSubnet2RouteTableAssociation",
                                           route_table_id=route_table.ref,
                                           subnet_id=private_subnets[1].ref),
            CfnSubnetRouteTableAssociation(self, "PrivateSubnet3RouteTableAssociation",
                                           route_table_id=route_table.ref,
                                           subnet_id=private_subnets[2].ref),
        ]

        ecs_security_group = CfnSecurityGroup(self, "ECSSecurityGroup",
                                              group_description="ECS Security Group",
                                              vpc_id=vpc.ref,
                                              security_group_ingress=[
                                                  {
                                                      "cidrIp": UncardConstants.vpc_cidr,
                                                      "ipProtocol": "tcp",
                                                      "fromPort": 8080,
                                                      "toPort": 8080
                                                  },
                                                  {
                                                      "cidrIp": UncardConstants.vpc_cidr,
                                                      "ipProtocol": "tcp",
                                                      "fromPort": 443,
                                                      "toPort": 443
                                                  }
                                              ],
                                              security_group_egress=[
                                                  {
                                                      "cidrIp": UncardConstants.vpc_cidr,
                                                      "ipProtocol": "tcp",
                                                      "fromPort": 443,
                                                      "toPort": 443
                                                  },
                                                  {
                                                      "destinationPrefixListId": "pl-6da54004",
                                                      "ipProtocol": "tcp",
                                                      "fromPort": 443,
                                                      "toPort": 443
                                                  }
                                              ])

        dynamodb_vpc_endpoint = CfnVPCEndpoint(self, "DynamodbVpcEndpoint",
                                               route_table_ids=[route_table.ref],
                                               service_name=Fn.join("", ["com.amazonaws.", region, ".dynamodb"]),
                                               vpc_id=vpc.ref,
                                               policy_document={"Statement": [{"Effect": "Allow",
                                                                               "Action": "*",
                                                                               "Principal": "*",
                                                                               "Resource": "*"}]
                                                                })

        s3_endpoint = CfnVPCEndpoint(self, "S3VpcEndpoint",
                                     route_table_ids=[route_table.ref],
                                     service_name=Fn.join("", ["com.amazonaws.", region, ".s3"]),
                                     vpc_id=vpc.ref)

        ecr_dkr_vpc_endpoint = CfnVPCEndpoint(self, "EcrDkrVpcEndpoint",
                                              service_name=Fn.join("", ["com.amazonaws.", region, ".ecr.dkr"]),
                                              vpc_id=vpc.ref,
                                              vpc_endpoint_type="Interface",
                                              subnet_ids=[private_subnets[0].ref, private_subnets[1].ref, private_subnets[2].ref],
                                              security_group_ids=[ecs_security_group.ref],
                                              private_dns_enabled=True)

        ecr_api_vpc_endpoint = CfnVPCEndpoint(self, "EcrApiVpcEndpoint",
                                              service_name=Fn.join("", ["com.amazonaws.", region, ".ecr.api"]),
                                              vpc_id=vpc.ref,
                                              vpc_endpoint_type="Interface",
                                              subnet_ids=[private_subnets[0].ref, private_subnets[1].ref, private_subnets[2].ref],
                                              security_group_ids=[ecs_security_group.ref],
                                              private_dns_enabled=True)

        logs_vpc_endpoint = CfnVPCEndpoint(self, "LogsVpcEndpoint",
                                           service_name=Fn.join("", ["com.amazonaws.", region, ".logs"]),
                                           vpc_id=vpc.ref,
                                           vpc_endpoint_type="Interface",
                                           subnet_ids=[private_subnets[0].ref, private_subnets[1].ref, private_subnets[2].ref],
                                           security_group_ids=[ecs_security_group.ref],
                                           private_dns_enabled=True)

        log_group = CfnLogGroup(self, "LogGroup", log_group_name=UncardConstants.log_group_name)

        ecs_cluster = CfnCluster(self, "Cluster",
                                 cluster_name=UncardConstants.ecs_cluster_name)

        task_definition = CfnTaskDefinition(self, "TaskDefinition",
                                            family="uncardservice",
                                            cpu="256",
                                            memory="512",
                                            network_mode="awsvpc",
                                            requires_compatibilities=["FARGATE"],
                                            execution_role_arn="arn:aws:iam::313059860395:role/UncardEcsExecutionRole",
                                            task_role_arn="arn:aws:iam::313059860395:role/UncardEcsTaskRole",
                                            container_definitions=[
                                                {
                                                    "name": "Uncard-Service",
                                                    "image": "313059860395.dkr.ecr.eu-west-1.amazonaws.com/uncard/service:latest",
                                                    "portMappings": [
                                                        {
                                                            "hostPort": 8080,
                                                            "containerPort": 8080,
                                                            "protocol": "http"
                                                        }
                                                    ],
                                                    "logConfiguration": {
                                                        "logDriver": "awslogs",
                                                        "options": {
                                                            "awslogs-group": "uncard-logs",
                                                            "awslogs-region": "eu-west-1",
                                                            "awslogs-stream-prefix": "awslogs-uncard-service"
                                                        }
                                                    },
                                                    "essential": True
                                                }
                                            ])

        self.__load_balancer = CfnLoadBalancer(self, "LoadBalancer",
                                               name=UncardConstants.load_balancer_name,
                                               scheme="internal",
                                               type="network",
                                               subnets=[private_subnets[0].ref, private_subnets[1].ref, private_subnets[2].ref]
                                               )
        target_group = CfnTargetGroup(self, "TargetGroup",
                                      name=UncardConstants.target_group_name,
                                      port=8080,
                                      protocol="TCP",
                                      target_type="ip",
                                      vpc_id=vpc.ref,  # might need to use the vpc_id provided by load_balancer
                                      health_check_interval_seconds=10,
                                      health_check_path="/",
                                      health_check_protocol="HTTP",
                                      healthy_threshold_count=3,
                                      unhealthy_threshold_count=3)

        listener = CfnListener(self, "Listener",
                               default_actions=[
                                   {
                                       "targetGroupArn": target_group.ref,
                                       "type": "forward"
                                   }
                               ],
                               load_balancer_arn=self.__load_balancer.ref,
                               port=80,
                               protocol="TCP")

        service = CfnService(self, "Service",
                             service_name=UncardConstants.service_name,
                             cluster=ecs_cluster.ref,
                             launch_type="FARGATE",
                             deployment_configuration={
                                 "maximumPercent": 200,
                                 "minimumHealthyPercent": 0
                             },
                             desired_count=0,
                             network_configuration={
                                 "awsvpcConfiguration": {
                                     "assignPublicIp": "DISABLED",
                                     "securityGroups": [ecs_security_group.ref],
                                     "subnets": [private_subnets[0].ref, private_subnets[1].ref, private_subnets[2].ref]
                                 }
                             },
                             task_definition=task_definition.ref,
                             load_balancers=[
                                 {
                                     "containerName": "Uncard-Service",
                                     "containerPort": 8080,
                                     "targetGroupArn": target_group.ref
                                 }
                             ])
        service.add_depends_on(listener)
        # TODO: enable vpc flow logs
        # TODO: create IAM role for vpc flow logs

        self.__vpc_link = CfnVpcLink(self, "VpcLink",
                                     name="ApiVpcLink",
                                     target_arns=[self.__load_balancer.ref])

        self.vpc_link_export = CfnOutput(self, "VpcLinkExport",
                                         value=self.__vpc_link.ref,
                                         export_name=UncardConstants.vpc_link_export)

        self.dns_name_export = CfnOutput(self, "DnsNameExport",
                                         value=self.__load_balancer.attr_dns_name,
                                         export_name=UncardConstants.load_balancer_dns_name_export)
