#!/usr/bin/env python3

from aws_cdk.core import App

from src.cognito_stack import UncardCognitoStack
from src.constants import UncardConstants
from src.dynamodb_stack import UncardDynamoDbStack
from src.ecr_stack import UncardEcrStack
from src.rest_api_stack import UncardRestApiStack
from src.roles_stack import UncardRolesStack
from src.vpc_stack import UncardVpcStack
from src.web_app_stack import UncardWebAppStack

app = App()

UncardRolesStack(app, "uncard-roles", env=UncardConstants.env)
UncardEcrStack(app, "uncard-ecr", env=UncardConstants.env)
UncardWebAppStack(app, "uncard-app", env=UncardConstants.env)
UncardDynamoDbStack(app, "uncard-dynamodb", env=UncardConstants.env)
UncardCognitoStack(app, "uncard-cognito", env=UncardConstants.env)
UncardVpcStack(app, "uncard-vpc", env=UncardConstants.env)
UncardRestApiStack(app, "uncard-rest-api", env=UncardConstants.env)

app.synth()
